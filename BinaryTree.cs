﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree
{
    public class Node
    {
        public int value;
        public Node left, right;


        public Node(int item)
        {
            value = item;
            left = right = null;
        }
    }

    public class Tree
    {
        public Node root;
        public Tree()
        {
            root = null;
        }
        public void Add(int value)
        {
            Node newNode = new Node(value);
            newNode.value = value;
            if (root == null)
                root = newNode;
            else
            {
                Node current = root;
                Node parent;
                while (true)
                {
                    parent = current;
                    if (value < current.value)
                    {
                        current = current.left;
                        if (current == null)
                        {
                            parent.left = newNode;
                            return;
                        }
                    }
                    else
                    {
                        current = current.right;
                        if (current == null)
                        {
                            parent.right = newNode;
                            return;
                        }
                    }
                }
            }
        }

        public void AscendingOrder(Node node)
        {
            if (node == null)
            {
                return;
            }
            AscendingOrder(node.left);
            Console.Write(node.value + " ");
            AscendingOrder(node.right);

        }
        public void DescendingOrder(Node node)
        {
            if (node == null)
            {
                return;
            }
            DescendingOrder(node.right);
            Console.Write(node.value + " ");
            DescendingOrder(node.left);


        }
        public int Searching(Node node, int value)
        {

            if (node == null)
            {
                return 0;
            }
            else if (Convert.ToInt32(value) == Convert.ToInt32(node.value))
            {
                return 1;
            }
            else if (Convert.ToInt32(value) < Convert.ToInt32(node.value))
            {
                return Searching(node.left, value);
            }
            else
            {
                return Searching(node.right, value);
            }


        }

        class Program
        {

            static void Main(string[] args)
            {

                int choice = 1, value;
                int[] values = new int[20];
                Tree tree = new Tree();
                Console.Write("enter the values maximum 20, 0 for stop ");
                for (int i = 0; i < 20; i++)
                {
                    values[i] = int.Parse(Console.ReadLine());
                    if (values[i] == 0)
                    {
                        break;
                    }
                    tree.Add(values[i]);
                }
                Console.Write("Tree is created");

                for (; choice != 0;)
                {
                    Console.WriteLine();
                    Console.WriteLine("Enter You option:");
                    Console.WriteLine("1. Asending order ");
                    Console.WriteLine("2. Descending order ");
                    Console.WriteLine("3. serching ");
                    Console.WriteLine();
                    choice = int.Parse(Console.ReadLine());
                    if (choice == 1)
                    {
                        tree.AscendingOrder(tree.root);
                    }
                    else if (choice == 2)
                    {
                        tree.DescendingOrder(tree.root);
                    }
                    else if (choice == 3)
                    {
                        Console.WriteLine("Enter the value for searching ");
                        value = int.Parse(Console.ReadLine());
                        value = tree.Searching(tree.root, value);
                        if (value == 1)
                        {
                            Console.WriteLine("The value is there in the tree ");
                            continue;
                        }
                        else
                        {
                            Console.WriteLine(" value not found in tree ");
                        }
                    }
                    else
                    {
                        Console.WriteLine("choose correct choice"); 
                    }
                }
            }
        }

    }
}


